
%% Load data

% This structure must include certain fields. See the README for details.  
load('./Example_datasets/EEG/Data_EEG_Isoluminant.mat');

%% Run ECOC

% Data.MultiCore = true ; % For enabling multicore processing
Data.Permutation = true;

Data = ECOC( Data );

%% Plot

colours = [1 0 0; 0 1 0; 1 0.6 0; 0 0 1];
labels = {'Red','Green','Orange','Turquoise'};

for i = 1:numel(Data.Time_decoder)
    accuracies(:,i) = diag(Data.CM_p(:,:,i)); 
    accuracies_c(:,i) = diag(Data.CMc_p(:,:,i));
end

figure('Position',[100 100 1200 900]);
for i = 1:4
    subplot(4,2,2*i-1);
    plot( Data.Time,Data.Data{i}(:,:,1) );
    title(labels{i});
    ylabel('Voltage (mV)');
    if i == 1
        legend('e'+string(1:8),'NumColumns',4);
    end
    if i == 4
        xlabel('Time (ms)');
    end
    axis('tight');
    ylim([-30 40]);  
end
subplot(4,2,[2,4]);
for i = 1:4
    plot( Data.Time_decoder, accuracies(i,:), 'Color', colours(i,:)); 
    hold on; 
end
ylabel('Probability');
title('ECOC accuracy');
axis('tight');
ylim([0 0.5]);
legend(labels);
subplot(4,2,[6,8]);
for i = 1:4
    plot( Data.Time_decoder, accuracies_c(i,:), 'Color', colours(i,:)); 
    hold on;
end
xlabel('Time (ms)');
ylabel('Probability');
title('Permuted baseline');
axis('tight');
ylim([0 0.5]);

