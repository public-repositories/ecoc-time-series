
%% Set up the data structure

% This structure must include certain fields. See the README for details.  
Folders = ["lying","standing","walking"];
FileSuffixes = 1:15;

% Data-related parameters
Data.NLabels = numel(Folders); 
Data.NFeatures = 6;
Data.TimeResolution = 500 ; 
Data.Time = 0:Data.TimeResolution:120000 ; 

for folderN_ = 1:numel(Folders)
    folder_ = Folders{folderN_};
    for fileSuffix_ = FileSuffixes
        path_ = ['./Example_datasets/WSN/' folder_ '/dataset' num2str(fileSuffix_) '.csv'];
        
        data_ = csvread(path_, 5,0); % 5 to 484
        time_ = data_(:,1); 
        sensors_ = data_(:,2:7);
        clearvars data_; 
        data_ = interp1(time_, sensors_, Data.Time, 'pchip');
        clearvars sensors_ time_ ;

        Data.Data{folderN_}(:,:,fileSuffix_) = data_.';
        clearvars path_ data_ ;

    end
    clearvars folderN_ folder_ ; 
end

%% Run ECOC

% Set up the ECOC parameters
Data.Algorithm = 'pseudoLinear';
Data.DecodingWindow_required = 5000 ; 
Data.MultiCore = true ; % For enabling multicore processing
Data.Permutation = true;

Data = ECOC( Data );

%% Plot
% colours = [1 0 0; 0 1 0; 0 0 1];
colours = colormap(hsv(Data.NLabels));
labels = Folders;

for i = 1:numel(Data.Time_decoder)
    accuracies(:,i) = diag(Data.CM_p(:,:,i)); 
    accuracies_c(:,i) = diag(Data.CMc_p(:,:,i));
end

figure('Position',[100 100 1200 900]);
for i = 1:Data.NLabels
    subplot(2*Data.NLabels,2,4*i+[-3 -1]);
    plot( Data.Time/1000,Data.Data{i}(:,:,1) );
    title(labels{i});
    ylabel('RSS output');
    if i == 1
        legend('e'+string(1:Data.NFeatures),'NumColumns',4);
    end
    if i == Data.NLabels
        xlabel('Time (s)');
    else
        xticklabels([]);
    end
    axis('tight');
    ylim([0 60]);  
end
subplot(2*Data.NLabels,2,2*(1:Data.NLabels));
for i = 1:Data.NLabels
    plot( Data.Time_decoder/1000, accuracies(i,:), 'Color', colours(i,:)); 
    hold on; 
end
xticklabels([]);
ylabel('Probability');
title('ECOC accuracy');
axis('tight');
ylim([0 1]);
legend(labels);
subplot(2*Data.NLabels,2,2*(Data.NLabels + (1:Data.NLabels)));
for i = 1:Data.NLabels
    plot( Data.Time_decoder/1000, accuracies_c(i,:), 'Color', colours(i,:)); 
    hold on;
end
xlabel('Time (s)');
ylabel('Probability');
title('Permuted baseline');
axis('tight');
ylim([0 1]);

