function Data = ECOC( Data )

    % Check for defaults
    if ( ~isfield(Data,'Algorithm') || isempty(Data.Algorithm) )
        Data.Algorithm = 'pseudoLinear';
    end
    if ( ~isfield(Data, 'Folds')  || isempty(Data.Folds) )
        Data.Folds = 5;
    end
    if ( ~isfield(Data, 'Design')  || isempty(Data.Design) )
        Data.Design = 'onevsall';
    end    
    if ( ~isfield(Data,'NReps')  || isempty(Data.NReps) )
        Data.NReps = 1;
    end
    if ( ~isfield(Data, 'SeedForTwister')  || isempty(Data.SeedForTwister) )
        Data.SeedForTwister = 123;
    end
    if ( ~isfield(Data, 'Permutation')  || isempty(Data.Permutation) )
        Data.Permutation = false;
    end
    if ( ~isfield(Data, 'Smoothing')  || isempty(Data.Smoothing) )
        Data.Smoothing = true;
    end
    if ( ~isfield(Data, 'MultiCore')  || isempty(Data.MultiCore) )
        Data.MultiCore = false;
    end

    
   % Set up the classifier
    rng(Data.SeedForTwister) ;
    switch lower(Data.Algorithm)
        case 'svm'
            algo = ...
                templateSVM('KernelFunction','rbf',...
                            'KernelScale','auto',...
                            'Standardize',1);
        case {'linear','pseudolinear','pseudoquadratic'}
            algo = ...
                templateDiscriminant('DiscrimType',Data.Algorithm);
        otherwise
            algo = ...
                templateDiscriminant('DiscrimType',Data.Algorithm);
    end
    d_win_n_ = ceil( Data.DecodingWindow_required / Data.TimeResolution ) ; 
    Data.DecodingWindow_actual = d_win_n_ * Data.TimeResolution ;
    Data.Time_decoder = Data.Time( d_win_n_:end );

    % Create a labelling variable
    y = [];
    for y_ = 1:Data.NLabels
        y = [y ; repmat( y_ , size(Data.Data{y_},3) , 1)];
    end
    
    % Classification
    for nT = 1:numel(Data.Time_decoder)
        
        X_ = [];
        for y_ = 1:Data.NLabels
            X_ = [X_ ; reshape(Data.Data{y_}(:,(1:d_win_n_)+(nT-1),:), Data.NFeatures*d_win_n_,[]).' ];
        end
        
        for i = 1:Data.NReps

            part_ = cvpartition(y,'k',Data.Folds); 
            pLabs_ = zeros(size(y));
            if Data.Permutation
                cLabs_ = zeros(size(y));
            end

            for k = 1:Data.Folds
                i_tr_ = part_.training(k) ; 
                tr_X_{k} = X_( i_tr_, :) ;
                tr_y_{k} = y( i_tr_ ) ;
                clearvars i_tr_;
                
                if Data.Permutation
                    tr_y_p_{k} = tr_y_{k}( randperm(length(tr_y_{k})) );
                end

                i_te_{k} = part_.test(k) ; 
                te_X_{k} = X_( i_te_{k}, :) ;
            end
            clearvars part_ ; 

            if Data.MultiCore
                b_p_ = Data.Permutation;
                s_desgn_ = Data.Design ;
                if ~b_p_
                    parfor k = 1:Data.Folds
                        mECOC_ = fitcecoc( tr_X_{k}, tr_y_{k},...
                                            'Coding',s_desgn_,'Learners',algo);
                        te_y_{k} = mECOC_.predict(te_X_{k});
                    end
                else
                    parfor k = 1:Data.Folds
                        mECOC_ = fitcecoc( tr_X_{k}, tr_y_{k},...
                                            'Coding',s_desgn_,'Learners',algo);
                        te_y_{k} = mECOC_.predict(te_X_{k});
                        mECOCc_ = fitcecoc(tr_X_{k}, tr_y_p_{k},...
                                            'Coding',s_desgn_,'Learners',algo);
                        te_y_p_{k} = mECOCc_.predict(te_X_{k});
                    end                    
                end

                clearvars b_p_ s_desgn_ ;
            else
                for k = 1:Data.Folds
                    mECOC_ = fitcecoc( tr_X_{k}, tr_y_{k},...
                                        'Coding',Data.Design,'Learners',algo);
                    te_y_{k} = mECOC_.predict(te_X_{k});
                    clearvars mECOC_ ;

                    if Data.Permutation
                        mECOCc_ = fitcecoc(tr_X_{k}, tr_y_p_{k},...
                                            'Coding',Data.Design,'Learners',algo);
                        te_y_p_{k} = mECOCc_.predict(te_X_{k});
                    end
                    clearvars mECOCc_ ; 
                end
            end
            clearvars tr_X_ tr_y_ tr_y_p_ te_X_; 

            for k = 1:Data.Folds
                pLabs_(i_te_{k}) = te_y_{k};
                if Data.Permutation
                    cLabs_(i_te_{k}) = te_y_p_{k};
                end
            end
            clearvars i_te_ te_y_ te_y_p_ ;
            
            % Outputs
            CM_ = confusionmat( y , pLabs_ ) ;
            Data.CM(:,:,nT,i) = CM_ ;
            CM_p_(:,:,nT,i) = diag( sum(CM_,2) ) \ CM_ ;
            clearvars CM_ pLabs_ ; 
            if Data.Permutation
                CMc_ = confusionmat( y , cLabs_ ) ;
                Data.CMc(:,:,nT,i) = CMc_ ;
                CMc_p_(:,:,nT,i) = diag( sum(CMc_,2) ) \ CMc_ ;
                clearvars CMc_ cLabs_ ; 
            end

        end
        
        clearvars X_ ;     
    end
    
    % Smoothing (optional)
    if Data.Smoothing
        s_win_n_ = d_win_n_;
        clearvars d_win_n_;
        
        for a = 1:Data.NLabels
            for b = 1:Data.NLabels
                for nT = 1:numel(Data.Time_decoder)
                    for i = 1:Data.NReps
                        Data.CM_p(a, b,:,i) =  ...
                            smooth( CM_p_(a,b,:,i), s_win_n_ ) ;
                        
                        if Data.Permutation
                            Data.CMc_p(a, b,:,i) =  ...
                                smooth( CMc_p_(a,b,:,i), s_win_n_ ) ;                            
                        end
                        
                    end
                end
            end
        end
        clearvars s_win_n_ CM_p_ CMc_p_ ;
    else
        Data.CM_p = CM_p_;
        clearvars CM_p_ ; 
        if Data.Permutation
            Data.CMc = CMc_p_;
            clearvars CMc_p_ ; 
        end
    end
    
end